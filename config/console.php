<?php

$config = [
    'id' => 'console',
    'bootstrap' => [
        'queue',
    ],
    'controllerMap' => [
        // Migrations for the specific project's module
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationTable' => '{{%migration}}',
            'interactive' => false,
            'migrationPath' => [
                '@app/modules/opendata/migrations',
            ],
        ],
         'access' => [
            'class' => \krok\access\AccessController::class,
            'userIds' => [
                1,
            ],
            'rules' => [
                \krok\auth\rbac\AuthorRule::class,
            ],
            'config' => [
                [
                    'label' => 'Open Data',
                    'name' => 'opendata',
                    'controllers' => [
                        'passport' => [
                            'label' => 'Passport',
                            'actions' => [],
                        ],
                        'set' => [
                            'label' => 'Set',
                            'actions' => [
                                'create',
                                'update',
                                'delete',
                                'import',
                                'data',
                                'delete-data',
                                'chart',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'modules' => [
        'opendata' => [
            'class' => app\modules\opendata\Module::class,
            'controllerNamespace' => 'app\modules\opendata\controllers\console',
            'inn' => '7710914971',
            'importUrl' => 'http://www.rosim.ru/opendata/list.csv', // Список доступных паспортов
        ],        
    ],
    'components' => [
        'urlManager' => [
            'class' => \yii\di\ServiceLocator::class,
            'components' => [
                'default' => require(__DIR__ . '/frontend/urlManager.php'),
                'backend' => require(__DIR__ . '/backend/urlManager.php'),
            ],
        ],
        'authManager' => [
            'class' => \yii\rbac\DbManager::class,
            'cache' => 'cache',
        ],
        'errorHandler' => [
            'class' => \yii\console\ErrorHandler::class,
        ],
    ],
];

return \yii\helpers\ArrayHelper::merge(require(__DIR__ . '/common.php'), $config);
