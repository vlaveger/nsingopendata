<?php

namespace app\modules\opendata\import\data;

use app\modules\opendata\dto\OpendataDataDTO;
use app\modules\opendata\dto\OpendataPropertyDTO;
use app\modules\opendata\dto\PassportSchemaDTO;
use app\modules\opendata\Module;
use Yii;
use yii\base\Exception;
use yii\helpers\StringHelper;

/**
 * Class ImportDataXml
 *
 * @package app\modules\opendata\import\data
 */
class ImportDataXml implements ImportDataInterface
{
    /**
     * @var Module
     */
    protected $module;

    /**
     * @var PassportSchemaDTO
     */
    protected $schema;
    
    /**
     * @var string
     */
    protected $delimiter;
    
    /**
     * @var string 
     */
    private $_boundaryElement = '';
    
    /**
     * @param string $data
     *
     * @return OpendataDataDTO[]|array
     * @throws Exception
     */
    public function import(string $data): array
    {
        if (!$this->schema) {
            throw new Exception('Passport schema is not loaded');
        }
        $reader = new \XMLReader();
        if (!$reader->xml($data, NULL, LIBXML_NOERROR)) {
            throw new Exception('$data isn\'t valid xml');
        }         
        $dto = new OpendataDataDTO();
        $list = [];
        $columnToProperty = [];
        while($reader->read()) {
            if($reader->nodeType == \XMLReader::ELEMENT && $reader->name == $this->_boundaryElement) {
                $reader->read();
                while (!($reader->nodeType == \XMLReader::END_ELEMENT && $reader->name == $this->_boundaryElement)) {
                    if ($reader->nodeType == \XMLReader::ELEMENT) {
                        $property = $reader->name;
                        $reader->read();
                        $value = $reader->value;
                        $dto->setPropertyValue($property, $value);
                    }
                    $reader->read();
                }
                $list[] = $dto;
            }
        }
        return $list;
    }

    /**
     * @param string $data
     *
     * @return PassportSchemaDTO
     */
    public function importSchema(string $data): PassportSchemaDTO
    {
        $this->module = Yii::$app->getModule('opendata');
        $this->schema = new PassportSchemaDTO();
        $xmldata = new \DOMDocument();
        $xmldata->preserveWhiteSpace = false; 

        $xml = $xmldata->loadXML($data);
        $elementTags = $xmldata->getElementsByTagName('element');
        $nextNodeName = $elementTags[0]->attributes[0]->value;
        $complexTypeTags = $xmldata->getElementsByTagName('complexType');
        $nodeNameWithSchema = '';
        foreach ($complexTypeTags as $complexTypeTag) {
            if ($complexTypeTag->attributes[0]->value == $nextNodeName) {
                foreach ($complexTypeTag->childNodes as $childNode) {
                    if ($childNode->tagName == 'xs:sequence') {
                        if ($childNode->firstChild->attributes[2]->name == 'maxOccurs' && $childNode->firstChild->attributes->length == 3) {
                            if ($childNode->firstChild->attributes[2]->value == '1') {
                                $nextNodeName = $childNode->firstChild->attributes[0]->value;
                            } elseif ($childNode->firstChild->attributes[2]->value == 'unbounded') {
                                $nodeNameWithSchema = $childNode->firstChild->attributes[0]->value;
                                $this->_boundaryElement = $childNode->firstChild->attributes[1]->value;
                            }
                        }
                    }
                }
            }
            if ($complexTypeTag->attributes[0]->value == $nodeNameWithSchema) {
                foreach ($complexTypeTag->childNodes as $childNode) {
                    if ($childNode->tagName == 'xs:sequence') {
                        foreach ($childNode->childNodes as $element) {
                            $this->schema->addProperty(new OpendataPropertyDTO(
                                    [
                                        'name' => $element->attributes[1]->value,
                                        'title' => $element->firstChild->childNodes[1]->nodeValue,
                                        'format' => str_replace('xs:', '', $element->attributes[0]->value),
                                    ]
                                )
                            );
                        }
                    }
                }
            }
        }

        return $this->schema;
    }

    /**
     * @param PassportSchemaDTO $schema
     *
     * @return PassportSchemaDTO
     */
    public function setSchema(PassportSchemaDTO $schema)
    {
        return $this->schema = $schema;
    }

    /**
     * @return PassportSchemaDTO
     */
    public function getSchema(): PassportSchemaDTO
    {
        return $this->schema;
    }

    /**
     * @param string $delimiter
     */
    public function setDelimiter(string $delimiter)
    {
        $this->delimiter = $delimiter;
    }
}
