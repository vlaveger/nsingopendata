<?php
/**
 * Created by PhpStorm.
 * User: elfuvo
 * Date: 01.08.17
 * Time: 11:33
 */

namespace app\modules\opendata\import\passport;

use app\modules\opendata\dto\OpendataPassportDTO;
use app\modules\opendata\Module;
use Yii;

/**
 * Class ImportPassportCsv
 *
 * @package app\modules\opendata\import\passport
 */
class ImportPassportCsv implements ImportPassportInterface
{
    /**
     * @var Module
     */
    protected $module;

    /**
     * @param $data string
     *
     * @return OpendataPassportDTO
     */
    public function import($data): OpendataPassportDTO
    {
        $dataDate = new \DateTime('0000-00-00');
        $structureDate = new \DateTime('0000-00-00');
        $this->module = Yii::$app->getModule('opendata');
        $rows = explode("\n", $data);
        $dto = new OpendataPassportDTO();
        if (count($rows) > 0) {
            unset($rows[0]); //удаляются заголовки столбцов
            foreach ($rows as $row) {
                if (!trim($row)){
                    continue;
                }
                list($property, $value) = explode($this->module->importCsvDelimiter, $row, 2);
                if (strpos($property, 'data') === 0) {
                    $dataAndSructure = $this->getDataAndSructure($property);
                    if ($dataAndSructure['dataDate'] > $dataDate) {
                        $dataDate = $dataAndSructure['dataDate'];
                        $structureDate = $dataAndSructure['structureDate'];
                        $dto->setUrl($value);
                    }
                }
                if (strpos($property, 'structure') === 0) {
                    $structureArray = explode('-', $property);
                    try {
                        $newStructureDate = new \DateTime($structureArray[1]);
                    } catch (Exception $e) {
                        $this->log($e->getMessage());
                    }
                    if ($newStructureDate == $structureDate) {
                        $structureDate = $newStructureDate;
                        $dto->setSchemaUrl($value);
                    }
                }
                switch ($property) {
                    case 'title':
                        $dto->setTitle($value);
                        break;
                    case 'identifier':
                        $dto->setIdentifier($value);
                        $dto->setCode($value);                        
                        break;
                    case 'description':
                        $dto->setDescription($value);
                        break;
                    case 'creator':
                        $dto->setOwner($value);
                        break;
                    case 'publishername':
                        $dto->setPublisherName($value);
                        break;
                    case 'publisherphone':
                        $dto->setPublisherPhone($value);
                        break;
                    case 'publishermbox':
                        $dto->setPublisherEmail($value);
                        break;
                    case 'created':
                        $dto->setCreatedAt($value);
                        break;
                    case 'modified':
                        $dto->setUpdatedAt($value);
                        break;
                    case 'subject':
                        $dto->setSubject($value);
                        break;
                    case 'provenance':
                        $dto->setChanges($value);
                        break;
                }
            }
        }
        return $dto;
    }
    
    /**
     * Извлекает из строки property версии набора данных и структуры
     * @param string $property
     * @return array
     */    
    private function getDataAndSructure(string $property) 
    {
        $dataAndStructureArray = explode('-', $property);
        try {
            $dataDate = new \DateTime($dataAndStructureArray[1]);
        } catch (Exception $e) {
            $this->log($e->getMessage());
        }
        if (strpos($property, 'structure')) {
            try {
                $structureDate = new \DateTime($dataAndStructureArray[3]);
            } catch (Exception $e) {
                $this->log($e->getMessage());
            }            
        }
        return ['dataDate' => $dataDate, 'structureDate' => $structureDate];
    }
}