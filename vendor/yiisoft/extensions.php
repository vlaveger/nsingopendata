<?php

$vendorDir = dirname(__DIR__);

return array (
  'yii2-developer/yii2-access' => 
  array (
    'name' => 'yii2-developer/yii2-access',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@krok/access' => $vendorDir . '/yii2-developer/yii2-access/src',
    ),
  ),
  'yii2-developer/yii2-application' => 
  array (
    'name' => 'yii2-developer/yii2-application',
    'version' => '0.0.3.0',
    'alias' => 
    array (
      '@krok/application' => $vendorDir . '/yii2-developer/yii2-application',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.10.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yii2-developer/yii2-system' => 
  array (
    'name' => 'yii2-developer/yii2-system',
    'version' => '1.0.7.0',
    'alias' => 
    array (
      '@krok/system' => $vendorDir . '/yii2-developer/yii2-system/src',
    ),
  ),
  'yii2-developer/yii2-password-eye' => 
  array (
    'name' => 'yii2-developer/yii2-password-eye',
    'version' => '0.0.1.0',
    'alias' => 
    array (
      '@krok/passwordEye' => $vendorDir . '/yii2-developer/yii2-password-eye',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'yii2-developer/yii2-flatpickr' => 
  array (
    'name' => 'yii2-developer/yii2-flatpickr',
    'version' => '0.0.5.0',
    'alias' => 
    array (
      '@krok/flatpickr' => $vendorDir . '/yii2-developer/yii2-flatpickr/src',
    ),
  ),
  'yii2-developer/yii2-extend' => 
  array (
    'name' => 'yii2-developer/yii2-extend',
    'version' => '1.0.9.0',
    'alias' => 
    array (
      '@krok/extend' => $vendorDir . '/yii2-developer/yii2-extend/src',
    ),
  ),
  'yii2-developer/yii2-select2' => 
  array (
    'name' => 'yii2-developer/yii2-select2',
    'version' => '0.1.1.0',
    'alias' => 
    array (
      '@krok/select2' => $vendorDir . '/yii2-developer/yii2-select2/src',
    ),
  ),
  'yii2-developer/yii2-configure' => 
  array (
    'name' => 'yii2-developer/yii2-configure',
    'version' => '1.2.1.0',
    'alias' => 
    array (
      '@krok/configure' => $vendorDir . '/yii2-developer/yii2-configure/src',
    ),
  ),
  'yii2-developer/yii2-auth' => 
  array (
    'name' => 'yii2-developer/yii2-auth',
    'version' => '2.0.0.0',
    'alias' => 
    array (
      '@krok/auth' => $vendorDir . '/yii2-developer/yii2-auth/src',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-queue' => 
  array (
    'name' => 'yiisoft/yii2-queue',
    'version' => '2.1.0.0',
    'alias' => 
    array (
      '@yii/queue' => $vendorDir . '/yiisoft/yii2-queue/src',
      '@yii/queue/amqp' => $vendorDir . '/yiisoft/yii2-queue/src/drivers/amqp',
      '@yii/queue/amqp_interop' => $vendorDir . '/yiisoft/yii2-queue/src/drivers/amqp_interop',
      '@yii/queue/beanstalk' => $vendorDir . '/yiisoft/yii2-queue/src/drivers/beanstalk',
      '@yii/queue/db' => $vendorDir . '/yiisoft/yii2-queue/src/drivers/db',
      '@yii/queue/file' => $vendorDir . '/yiisoft/yii2-queue/src/drivers/file',
      '@yii/queue/gearman' => $vendorDir . '/yiisoft/yii2-queue/src/drivers/gearman',
      '@yii/queue/redis' => $vendorDir . '/yiisoft/yii2-queue/src/drivers/redis',
      '@yii/queue/sync' => $vendorDir . '/yiisoft/yii2-queue/src/drivers/sync',
      '@yii/queue/sqs' => $vendorDir . '/yiisoft/yii2-queue/src/drivers/sqs',
    ),
  ),
  'yiisoft/yii2-httpclient' => 
  array (
    'name' => 'yiisoft/yii2-httpclient',
    'version' => '2.0.11.0',
    'alias' => 
    array (
      '@yii/httpclient' => $vendorDir . '/yiisoft/yii2-httpclient/src',
    ),
  ),
  'yii2-developer/yii2-sms' => 
  array (
    'name' => 'yii2-developer/yii2-sms',
    'version' => '0.0.5.0',
    'alias' => 
    array (
      '@krok/sms' => $vendorDir . '/yii2-developer/yii2-sms/src',
    ),
  ),
  'yii2-developer/yii2-queue' => 
  array (
    'name' => 'yii2-developer/yii2-queue',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@krok/queue' => $vendorDir . '/yii2-developer/yii2-queue/src',
    ),
  ),
  'yii2-developer/yii2-backup' => 
  array (
    'name' => 'yii2-developer/yii2-backup',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@krok/backup' => $vendorDir . '/yii2-developer/yii2-backup/src',
    ),
  ),
  'yii2-developer/yii2-bootbox' => 
  array (
    'name' => 'yii2-developer/yii2-bootbox',
    'version' => '0.0.2.0',
    'alias' => 
    array (
      '@krok/bootbox' => $vendorDir . '/yii2-developer/yii2-bootbox',
    ),
  ),
  'yii2-developer/yii2-editor' => 
  array (
    'name' => 'yii2-developer/yii2-editor',
    'version' => '2.0.0.0',
    'alias' => 
    array (
      '@krok/editor' => $vendorDir . '/yii2-developer/yii2-editor',
    ),
  ),
  'yii2-developer/yii2-catch-all' => 
  array (
    'name' => 'yii2-developer/yii2-catch-all',
    'version' => '2.0.0.0',
    'alias' => 
    array (
      '@krok/catchAll' => $vendorDir . '/yii2-developer/yii2-catch-all/src',
    ),
  ),
  'yii2-developer/yii2-transliterate' => 
  array (
    'name' => 'yii2-developer/yii2-transliterate',
    'version' => '0.0.1.0',
    'alias' => 
    array (
      '@krok/transliterate' => $vendorDir . '/yii2-developer/yii2-transliterate/src',
    ),
  ),
  'yii2-developer/yii2-content' => 
  array (
    'name' => 'yii2-developer/yii2-content',
    'version' => '2.0.0.0',
    'alias' => 
    array (
      '@krok/content' => $vendorDir . '/yii2-developer/yii2-content/src',
    ),
  ),
  'yii2-developer/yii2-datetime-formatter' => 
  array (
    'name' => 'yii2-developer/yii2-datetime-formatter',
    'version' => '0.0.3.0',
    'alias' => 
    array (
      '@krok/datetimeFormatter' => $vendorDir . '/yii2-developer/yii2-datetime-formatter/src',
    ),
  ),
  'yii2-developer/yii2-grid' => 
  array (
    'name' => 'yii2-developer/yii2-grid',
    'version' => '0.0.2.0',
    'alias' => 
    array (
      '@krok/grid' => $vendorDir . '/yii2-developer/yii2-grid/src',
    ),
  ),
  'yii2-developer/yii2-mailer' => 
  array (
    'name' => 'yii2-developer/yii2-mailer',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@krok/mailer' => $vendorDir . '/yii2-developer/yii2-mailer/src',
    ),
  ),
  'yii2-developer/yii2-maxlength' => 
  array (
    'name' => 'yii2-developer/yii2-maxlength',
    'version' => '0.0.2.0',
    'alias' => 
    array (
      '@krok/maxlength' => $vendorDir . '/yii2-developer/yii2-maxlength/src',
    ),
  ),
  'yii2-developer/yii2-filesystem' => 
  array (
    'name' => 'yii2-developer/yii2-filesystem',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@krok/filesystem' => $vendorDir . '/yii2-developer/yii2-filesystem/src',
    ),
  ),
  'yii2-developer/yii2-glide' => 
  array (
    'name' => 'yii2-developer/yii2-glide',
    'version' => '2.1.3.0',
    'alias' => 
    array (
      '@krok/glide' => $vendorDir . '/yii2-developer/yii2-glide/src',
    ),
  ),
  'yii2-developer/yii2-clock' => 
  array (
    'name' => 'yii2-developer/yii2-clock',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@krok/clock' => $vendorDir . '/yii2-developer/yii2-clock',
    ),
  ),
  'yii2-developer/yii2-analytics' => 
  array (
    'name' => 'yii2-developer/yii2-analytics',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@krok/analytics' => $vendorDir . '/yii2-developer/yii2-analytics',
    ),
  ),
  'yii2-developer/yii2-paper-dashboard' => 
  array (
    'name' => 'yii2-developer/yii2-paper-dashboard',
    'version' => '1.6.0.0',
    'alias' => 
    array (
      '@krok/paperdashboard' => $vendorDir . '/yii2-developer/yii2-paper-dashboard/src',
    ),
  ),
  'yii2-developer/yii2-reverse-proxy' => 
  array (
    'name' => 'yii2-developer/yii2-reverse-proxy',
    'version' => '0.0.4.0',
    'alias' => 
    array (
      '@krok/reverseProxy' => $vendorDir . '/yii2-developer/yii2-reverse-proxy/src',
    ),
  ),
  'yii2-developer/yii2-robots' => 
  array (
    'name' => 'yii2-developer/yii2-robots',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@krok/robots' => $vendorDir . '/yii2-developer/yii2-robots/src',
    ),
  ),
  'yii2-developer/yii2-sentry' => 
  array (
    'name' => 'yii2-developer/yii2-sentry',
    'version' => '2.0.1.0',
    'alias' => 
    array (
      '@krok/sentry' => $vendorDir . '/yii2-developer/yii2-sentry/src',
    ),
  ),
  'yii2-developer/yii2-tinymce' => 
  array (
    'name' => 'yii2-developer/yii2-tinymce',
    'version' => '2.0.0.0',
    'alias' => 
    array (
      '@krok/tinymce' => $vendorDir . '/yii2-developer/yii2-tinymce',
    ),
  ),
  'yii2-developer/yii2-tinymce-uploader' => 
  array (
    'name' => 'yii2-developer/yii2-tinymce-uploader',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@krok/tinymce/uploader' => $vendorDir . '/yii2-developer/yii2-tinymce-uploader/src',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'yiisoft/yii2-redis' => 
  array (
    'name' => 'yiisoft/yii2-redis',
    'version' => '2.0.9.0',
    'alias' => 
    array (
      '@yii/redis' => $vendorDir . '/yiisoft/yii2-redis/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.14.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yii2-developer/yii2-language' => 
  array (
    'name' => 'yii2-developer/yii2-language',
    'version' => '1.1.1.0',
    'alias' => 
    array (
      '@krok/language' => $vendorDir . '/yii2-developer/yii2-language/src',
    ),
  ),
  'yii2-developer/yii2-meta' => 
  array (
    'name' => 'yii2-developer/yii2-meta',
    'version' => '1.2.3.0',
    'alias' => 
    array (
      '@krok/meta' => $vendorDir . '/yii2-developer/yii2-meta/src',
    ),
  ),
  'unclead/yii2-multiple-input' => 
  array (
    'name' => 'unclead/yii2-multiple-input',
    'version' => '2.10.0.0',
    'alias' => 
    array (
      '@unclead/multipleinput/examples' => $vendorDir . '/unclead/yii2-multiple-input/examples',
      '@unclead/multipleinput' => $vendorDir . '/unclead/yii2-multiple-input/src',
      '@unclead/multipleinput/tests' => $vendorDir . '/unclead/yii2-multiple-input/tests',
    ),
  ),
  'dpodium/yii2-colorpicker' => 
  array (
    'name' => 'dpodium/yii2-colorpicker',
    'version' => '0.2.0.0',
    'alias' => 
    array (
      '@dpodium/colorpicker' => $vendorDir . '/dpodium/yii2-colorpicker',
    ),
  ),
  'contrib/yii2-charts' => 
  array (
    'name' => 'contrib/yii2-charts',
    'version' => '0.2.0.0',
    'alias' => 
    array (
      '@zima/charts' => $vendorDir . '/contrib/yii2-charts',
    ),
  ),
);
